# homework



## Getting started

- Elsőként a .releaserc.yml hoztam létre, a verziózás és a tag-ek miatt.
  https://levelup.gitconnected.com/semantic-versioning-and-release-automation-on-gitlab-9ba16af0c21

- .gitlab-ci.yml "release" és "tag_and_build" stage:

    a pipeline csak akkor indul el ha a "main branch-re merge-elünk" és a semantic verziózás szabályai szerint megfelelő commit tag-et használunk.
    ilyenkor a generalt verzióval "tag-eljük" az image-eket.
    https://semantic-release.gitbook.io/semantic-release/#commit-message-format

- Létrehoztam egy alap FlaskApp-ot a Solr eléréséhez:

  A szükséges modulok importálása után, változókba mentettem a Solr eléréshéhez szükséges URL-t, felhasználónevet és jelszót.

  Az első "route" kezeli a "GET" és "POST" kéréseket, a keresések betöltéséhez.

  urllib.request modult-t használom az autentikációhoz.

  A második "route" kezeli a .json fájl feltöltését solr-be. Itt már a pysolr modult használtam autentikációhoz és feltöltéshez.

  index.html -ben jinja -val behelyettesítem a keresési eredményeket a html kódba.

  upload.html -ben flask modulok segítségével töltöm fel a fájlt a static/files mappába.
  
- Dockerfile.flask létrehozza flask-image-et. Első lépésben előkészíti a virtuális környezetet, telepíti a dependeciákat, a második fázisban átmásolom a szükséges fájlokat, csinálok egy új felhasználót és elindítom a flask applikációt.

  Dockerfile.flask -ben az entrypoint.sh script segítsségével addom meg a konténer ENTRYPONT-ját ami a python virtuális környezetet indítja el

- Dockerfile.solr létrehozza a solr-image-et átmásolja a security.json fájlt az autentikáció létrehozásához.

- docker-compose.yml egy közös network-ön (bridge) létrehozza a flask és solr konténereket, kinyitja a portokat, létrehoz egy core-t

## Futtatás

- docker compose -f "docker-compose.yml" up -d --build
- böngészőben miután megnyitjuk a flask appot (localhost:5000), az "UPLOAD JSON" linkre kattintva feltöltjük a "Solr_import_v2.json" fájlt (jelenleg csak ezt a fájlt imseri fel)
- Ezután a kezdő oldalon az üres mező mellett a "SEARCH" gombra kattintva kilistázza az összes találatot.
