from flask import Flask, render_template, request, redirect, url_for
import urllib.request
import simplejson
import json
import pysolr
from flask_wtf import FlaskForm
from wtforms import FileField, SubmitField
from werkzeug.utils import secure_filename
import os
from wtforms.validators import InputRequired


app = Flask(__name__)
app.config['SECRET_KEY'] = "supersecretkey"
app.config['UPLOAD_FOLDER'] = "static/files"

BASE_PATH = "http://solr:8983/solr/media/select?debugQuery=false&defType=edismax&q.op=AND&rows=250&qf=id%20programmeProperties_programmeTitle_s%20programmeProperties_seriesTitle_s%20programmeProperties_genre_s%20programmeProperties_season_s&useParams=&wt=json&q="

LOCAL_PATH = "http://localhost:8983/solr/media/select?debugQuery=false&defType=edismax&q.op=AND&rows=250&qf=id%20programmeProperties_programmeTitle_s%20programmeProperties_seriesTitle_s%20programmeProperties_genre_s%20programmeProperties_season_s&useParams=&wt=json&q="

USER = "solr"
PASSWORD = "SolrRocks"


@app.route('/', methods=['POST', 'GET'])
def index():
    query = None
    numresults = None
    results = None

    if request.method == "POST":
        query = request.form["searchTerm"]

        if query is None or query == "":
            query = "*:*"

        passman = urllib.request.HTTPPasswordMgrWithDefaultRealm()
        passman.add_password(None, BASE_PATH, USER, PASSWORD)
        authhandler = urllib.request.HTTPBasicAuthHandler(passman)
        opener = urllib.request.build_opener(authhandler)
        urllib.request.install_opener(opener)

        url = f"{BASE_PATH}{query}".replace(" ", "%20")
        connection = urllib.request.urlopen(url)
        response = simplejson.load(connection)
        numresults = response['response']['numFound']
        results = response['response']['docs']

    return render_template("index.html", query=query, numresults=numresults, results=results)


solr = pysolr.Solr("http://solr:8983/solr/media", always_commit=True, timeout=10, auth=(USER, PASSWORD))


class UploadFileForm(FlaskForm):
    file = FileField("File", validators=[InputRequired()])
    submit = SubmitField("Upload File")


@app.route('/upload', methods=["GET", "POST"])
def upload():
    form = UploadFileForm()
    if form.validate_on_submit():
        file = form.file.data
        file.save(os.path.join(os.path.abspath(os.path.dirname(__file__)), app.config['UPLOAD_FOLDER'], secure_filename(file.filename)))
        opened = open('static/files/Solr_import_v2.json')
        solr.add(json.load(opened))
        return redirect(url_for('index'))
    return render_template('upload.html', form=form)


if __name__ == "__main__":
    app.run(host="0.0.0.0")
